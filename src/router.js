import { createRouter, createWebHistory } from "vue-router";
import ProfilesList from "./pages/ProfilesList.vue";
import ProfileDetails from "./pages/ProfileDetails.vue";
import NotFound from "./pages/NotFound.vue";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: "/", redirect: "/profiles" },
    { path: "/profiles", component: ProfilesList },
    { path: "/profiles/:id", component: ProfileDetails, props: true },
    { path: "/:notFound(.*)", component: NotFound },
  ],
  scrollBehavior(_, _2, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    return { left: 0, right: 0 };
  },
});

export default router;
