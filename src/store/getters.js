export default {
  users(state) {
    return state.users;
  },
  isOpened(state) {
    return state.isOpened;
  },
};
