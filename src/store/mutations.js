export default {
    addUsers(state, payload) {
        state.users.push(...payload);
    },
    isOpen(state) {
        state.isOpened = true;
    },
    verifyProfile(state, payload) {
        const selectedUser = state.users.find(user => user.id === payload);
        const index = state.users.indexOf(selectedUser);
        state.users[index]['isVerified'] = true;
    }
};