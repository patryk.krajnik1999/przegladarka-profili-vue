export default {
  async addUsers(context, payload) {
    const response = await fetch("https://randomuser.me/api/?results=5");
    const responseData = await response.json();

    if (!response.ok) {
      const error = new Error(responseData.members || "Failed to fetch");
      throw error;
    }

    const users = [];
    let isNotUnique = true;
    for (const res in responseData["results"]) {
      isNotUnique = true;
      while (isNotUnique) {
        const address =
          "Street: " +
          responseData["results"][res]["location"]["street"]["name"] +
          " " +
          responseData["results"][res]["location"]["street"]["number"] +
          ", Postcode:" +
          responseData["results"][res]["location"]["postcode"] +
          ", City:" +
          responseData["results"][res]["location"]["city"] +
          ", Country:" +
          responseData["results"][res]["location"]["country"];
        const birthDate = responseData["results"][res]["dob"]["date"].substr(
          0,
          10,
        );
        const registerDate = responseData["results"][res]["registered"][
          "date"
        ].substr(0, 10);
        const registerTime = responseData["results"][res]["registered"][
          "date"
        ].substr(11, 8);
        const firstName = responseData["results"][res]["name"]["first"];
        const lastName = responseData["results"][res]["name"]["last"];
        const picture = responseData["results"][res]["picture"];
        const email = responseData["results"][res]["email"];
        const uuId = responseData["results"][res]["login"]["uuid"];
        const num = parseInt(payload) + parseInt(res);
        const id = 'p' + num;
        const user = {
          id: id,
          uuId: uuId,
          address: address,
          birthDate: birthDate,
          registerDate: registerDate,
          registerTime: registerTime,
          firstName: firstName,
          lastName: lastName,
          picture: picture,
          email: email,
          isVerified: false,
        };
        isNotUnique = users.find((el) => el.uuId === user.uuId) === "undefined" ? true : false;
        if (!isNotUnique) {
          users.push(user);
        }
      }
    }
    context.commit("addUsers", users);
  },
  isOpen(context) {
    context.commit('isOpen');
  },
  verifyProfile(context, payload) {
    context.commit('verifyProfile', payload);
  }
};
